# pylint: disable=missing-docstring,invalid-name
from conans import CMake, ConanFile, tools


class GodotcppConan(ConanFile):
    name = "Godot-cpp"
    version = "3.1"
    license = "MIT"
    author = "Godot native tools"
    url = "https://gitlab.com/mdavezac-conan/godot-cpp"
    homepage = "https://github.com/GodotNativeTools/godot-cpp.git"
    description = "C++ bindings for the Godot script API"
    topics = ("Game Engine", "C++")
    settings = "os", "compiler", "build_type", "arch"
    generators = "cmake"
    exports_sources = "patch.diff"

    def source(self):
        git = tools.Git(folder="godot")
        git.clone(self.homepage, branch=GodotcppConan.version, args="--recursive")
        tools.patch("godot", patch_file="patch.diff")

    def configured_cmake(self):
        cmake = CMake(self)
        cmake.definitions["CMAKE_POSITION_INDEPENDENT_CODE"] = True
        cmake.configure(source_folder="godot")
        return cmake

    def build(self):
        cmake = self.configured_cmake()
        cmake.build()

    def package(self):
        self.copy("gdnative/*", src="godot/godot_headers", dst="include/godot")
        self.copy("android/*", src="godot/godot_headers", dst="include/godot")
        self.copy("net/*", src="godot/godot_headers", dst="include/godot")
        self.copy("pluginscript/*", src="godot/godot_headers", dst="include/godot")
        self.copy("nativescript/*", src="godot/godot_headers", dst="include/godot")
        self.copy("videodecoder/*", src="godot/godot_headers", dst="include/godot")
        self.copy("images/*", src="godot/godot_headers", dst="include/godot")
        self.copy("arvr/*", src="godot/godot_headers", dst="include/godot")
        self.copy(
            "gdnative_api_struct.gen.h", src="godot/godot_headers", dst="include/godot"
        )
        self.copy("core/*", src="godot/include", dst="include/godot")
        self.copy("*", src="godot/include/gen", dst="include/godot")
        self.copy("libgodot-cpp.*", src="godot/bin", dst="lib")

    def package_info(self):
        self.cpp_info.libs = ["godot-cpp"]
        self.cpp_info.includedirs = ["include/godot", "include/godot/core"]
